package controllers;

import models.Billettrie;
import helmo.nhpack.NHPack;
import java.util.ArrayList;
import models.Reservation;

/**
 * Cette classe pilote les deux fenêtres.
 */
public class MainWindowController {
    private String message;
    private final Billettrie  helmoFC;
    private Reservation demande;

    public MainWindowController() {
        this.helmoFC = new Billettrie();
        this.demande = null;
    }
    
    /**
     * Cette méthode le texte associé à chaque tribune.
     * @return un tableau de 4 Strings.
     */
    public String[] getTribune() {
        return this.helmoFC.getTribune();
    }

    /**
     * Méthode appelée depuis l'un des quatre boutons.
     * Si la tribune indiceTribune possède des disponibilités, la méthode affiche la boite de réservation.
     * Sinon, un message d'erreur est affiché.
     * @param indiceTribune le numéro de la tribune.
     */
    public void nouvelleDemande(int indiceTribune) {
        this.message = "";
        if(this.helmoFC.possedeEncoreDesPlaces(indiceTribune)) {
            demande = new Reservation(indiceTribune);
            NHPack.getInstance().loadWindow("views.demande.xml", this); 
            NHPack.getInstance().showWindow("views.demande.xml");
            return;
        }
        this.message = "Cette réservation est impossible";
        
    }

    /**
     * @return le nom encodé lors de la réservation
     *  
     */
    public String getNom() {
        if(this.demande!=null) return demande.getNom();
        return null;
    }

    /** 
     * Lit le nom encodé dans la fenêtre de réservation.
     * @param nom
     */
    public void setNom(String nom) {
        if("".equals(nom)){
            demande.setNom(null);
            return;
        }
        demande.setNom(nom);
    }

    /**
     * @return la quantité de la réservation
     */
    public int getQuantite() {
        if(this.demande !=null) return this.demande.getNbPlace();
        return 0;
    }

    /** 
     * Lit la quantité encodée dans la fenêtre de réservation.
     * @param quantite
     */
    public void setQuantite(int quantite) {
        if(quantite == 0) return;
        this.demande.setNbPlace(quantite);
    }

    /** Méthode appelée lorsque l'utilisateur valide la réservation.
     *  Si la réservation est valable, cette méthode ferme la boite de dialogue et met à jour
     *  la fenêtre principale.
     */
    public void tenterAjout() {
        if(this.helmoFC.tenterAjout(demande)) {
            this.helmoFC.setRecette(demande.calculerPrix());
            this.message = ""+ this.demande +" . La recette totale est de "+ this.helmoFC.getRecette()+ " euros ";
            NHPack.getInstance().closeWindow("views.demande.xml"); 
            NHPack.getInstance().refreshMainWindow();
            return;
        } 
        this.message = "Cette réservation est impossible";
      
    }

    /** Méthode appelée lorsque l'utilisateur modifie le nombre de place réservées.
     */
    public void verifierPlaces() {
        if(this.helmoFC.peutRecevoir(demande)) {
            this.message = "La reservaion est possible";
            return;
        } 
        this.message = "Cette réservation est impossible";
        
    }
    
    /** 
     * @return un message à afficher dans une fenêtre.
     */
    public String getMessage() {
        return this.message;
    }
    public ArrayList getListReservation(){
       return this.helmoFC.afficherReservation();
    }
    public void textBoxListeReservation(){
        if(this.helmoFC.nbReservation()> 0){
            NHPack.getInstance().loadWindow("views.listereservation.xml", this); 
            NHPack.getInstance().showWindow("views.listereservation.xml");
            return ;
        }
        this.message = "Pas de reservation enregistré";
    
    }
}
