/**
 *      Tribune
 *  Genere une tribune
 * 
 * @param numero de la tribune
 * @param placeVendue le nombre de place vendue
 * @param capacite de la tribunne
 * 
 * @author Somboom TUNSAJAN (G3)
 */
package models;

public class Tribune {
    private String nom;
    private int placeVendue;
    private int capacite;
    
    public Tribune(String nom, int capacite){
        this.nom = nom;
        this.setCapacite(capacite);
        this.placeVendue = 0;
        
    }
    
    /**
     * 
     * @param numero affecte un numero à la tribunne 
     */
    /*private void setNumero(int numero){
        if(numero <=0) return; /* Exception */
        //this.numero=numero;
    //}*/
    
    /**
     * 
     * @param capacite affecte une capacite a la tribuen 
     */
    private void setCapacite(int capacite){
        if(capacite <=0) return; /* Exception */
        this.capacite = capacite;        
    }
    
    /**
     * 
     * @return si il y a encore des place disponible ou pas  
     */
    public boolean isDisponible(){
        return this.getPlaceDisponible()> 0;
    }
    public int getPlaceDisponible(){
        return this.capacite-this.placeVendue;
    }
    public void placeReserve(int nbPlace){
        this.placeVendue+=nbPlace;
    }
    public String getNom(){
        return this.nom;
    }
    @Override
    public String toString(){
        
        if(this.isDisponible()) return ""+this.getNom()+" "+this.placeVendue + "/"+ this.capacite;
        else return ""+"COMPLET";
        
    }
}
