/**
 *          Billettrie
 * 
 * @param Tribune[] contient les tribunnes à remplir
 * @param recette contient le montant total des vente des places
 * @param listeReservaion contient les reservations effectuées
 * @author Somboom TUNSAJAN (G3)
 * 
 */
package models;

import java.util.ArrayList;

public class Billettrie {
    private Tribune[] listeTribune;
    private int recette;
    private ArrayList<Reservation>  listeReservation;
    

    public Billettrie(){
        this.setRecette(0);
        this.setListeReservation();
        this.setListeTribune();
    }
    /**
     *  Initialise une liste de reservation vide
     */
    private void setListeReservation(){
        this.listeReservation = new ArrayList();
    }
    /**
     * 
     * @param montant augmante la recette au fur et a mesure des ventes 
     */
    public final void setRecette(int montant){
        this.recette+=montant;
    }
    /**
     *  Permet d initialiser la liste des tribune
     */
    private void setListeTribune(){
       this.listeTribune = new Tribune[4];
       this.listeTribune[0] = new Tribune( "BLUE", 15);
       this.listeTribune[1] = new Tribune ("RED", 20);
       this.listeTribune[2] = new Tribune("YELLOW", 15);
       this.listeTribune[3] = new Tribune("GREEN", 20);
    }
    
    /**
     * 
     * @return Renvois le nom de la tribune et l etat de celle-ci
     */
    public String[] getTribune(){
        String nomTribune[] = new String[4];
        for(int i = 0; i<nomTribune.length; i++) nomTribune[i] = this.listeTribune[i]+"";
        return nomTribune;
    }
    public Tribune getUneTribune(int i){
        if(i<0) return null;
        return this.listeTribune[i];
    }
    /**
     * 
     * @param indiceTribune numero de la tribune a tester
     * @return renvois true si des places sont encore disponible
     */
    public boolean possedeEncoreDesPlaces(int indiceTribune){
        return this.listeTribune[indiceTribune].isDisponible();
       
    } 
    /**
     * Check si la tribune peut recevoir le nombre de place demandé
     * @param demande
     * @return 
     */
    public boolean peutRecevoir(Reservation demande){
        
        return (listeTribune[demande.getNumeroTribune()].getPlaceDisponible()>=demande.getNbPlace());
    }
    /**
     * 
     * @param demande reservation tamporaire
     * @return l etat de la reservation
     */
    public boolean tenterAjout(Reservation demande) {
        if(demande.getNbPlace() == 0|| demande.getNom() == null) return false;
        if(this.peutRecevoir(demande)){
            // Marque les place reserve dans la tribunne demandée
            this.listeTribune[demande.getNumeroTribune()].placeReserve(demande.getNbPlace());
            this.listeReservation.add(demande);
            return true;
        }        
        return false;
    }
    /**
     * 
     * @return Renvois la recette de la biettrie 
     */
    public int getRecette(){
        return this.recette;
    }
    /**
     * 
     * @return Renvois le nombre de reservations enregostrée
     */
    public int nbReservation(){
        return this.listeReservation.size();
    }
    /**
     * Genere une liste de reservation sous forme de String
     * @return la liste des reservations
     */
    public ArrayList afficherReservation(){
        return this.listeReservation;
        
    }
}
