/**
 *      Reservation
 *  class permettant de generer une reservation
 * @param nom  nom du client
 * @param nbPlace le nombre de places reservées
 * @param numeroTribune contient le numero de la tribune 
 * pour lequel on a reservé des places
 * 
 * @author Somboom TUNSAJAN
 */
package models;


public class Reservation implements Configuration{
    private String nom;
    private int nbPlace;
    private final int numeroTribune;
  
    
    public Reservation(int numeroTribune){
        this.nom = null;
        this.nbPlace = 0;
        this.numeroTribune = numeroTribune;
    }
    
    /**
     * @return le nombre de places reservé lors d une reservation
     * 
     */
    public int getNbPlace(){
        return this.nbPlace;
    }
    
    /**
     * 
     * @return le numero de la tribune ou les places sont reservé 
     */
    public int getNumeroTribune(){
        return this.numeroTribune;
    }
    
    /**
     * 
     * @param nom du client  
     */
    public void setNom(String nom){
        this.nom=nom;
        
    }
    
    /**
     * 
     * @return revois le nom du client 
     */
    public String getNom(){
        return this.nom;
    }
    
    /**
     * 
     * @param nbPlace que l on veut reservés 
     */
    public void setNbPlace(int nbPlace){
        if(nbPlace<=0) return;/*Exception */ 
        this.nbPlace = nbPlace;
    }
    
    /**
     *  
     * @return le montant de la reservation effectue en fonction des 
     * places reservés
     */
    public int calculerPrix(){
        return this.getNbPlace()*PRIX_TICKET[this.getNumeroTribune()];
    }
    @Override
    public String toString(){
        return "Le client "+ this.getNom() + " à reservé "+this.getNbPlace() + " places en T"+(this.numeroTribune+1) ;
    }
    
}
